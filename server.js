const http = require('http');
let port = 4000;

http.createServer((request, response) => {
/*
	* Create a simple server and the following routes with their corresponding HTTP methods and responses:
		- If the url is http://localhost:4000/, send a response Welcome to Booking System
		- If the url is http://localhost:4000/profile, send a response Welcome to your profile!
		- If the url is http://localhost:4000/courses, send a response Here’s our courses available
		- If the url is http://localhost:4000/addcourse, send a response Add a course to our resources
*/	
	let courses = [0];
	// console.log(typeof courses);

	if(request.url === "/" && request.method ==="GET"){
			response.writeHead(200, {"Content-Type" : "application/json"})
			response.write(`Welcome to Booking System!`);
			response.end()
	} 
	else if(request.url === "/profile" && request.method ==="GET"){
			response.writeHead(200, {"Content-Type" : "application/json"})
			response.write(`Welcome to your Profile!`);
			response.end()
	}
	else if(request.url === "/courses" && request.method ==="GET"){
			response.writeHead(200, {"Content-Type" : "application/json"})
			response.write(`"Here’s our courses available!`);
			response.end()
	}

	else if(request.url === "/addcourse" && request.method === "POST"){
		let requestBody ="";
		request.on('data', (data) => {
			requestBody += data;
		})
		console.log(requestBody);

		request.on('end', () => {
			console.log(typeof requestBody); //string
			console.log(requestBody);

			requestBody = JSON.parse(requestBody);
			console.log(typeof requestBody); //object

			let newCourse = {
				"title" : requestBody.title
			};

			console.log(newCourse);
			console.log(typeof newCourse);
			courses.push(newCourse);

			response.writeHead(201, {'Content-Type' : "application/json"});
			response.write(`Add a course to our resources`);
			// response.write(JSON.stringify(newCourse));
			response.end();
		})
	}
/*
	* Create a simple server and the following routes with their corresponding HTTP methods and responses:
		- If the url is http://localhost:4000/updatecourse, send a response Update a course to our resources
		- If the url is http://localhost:4000/archivecourses, send a response Archive courses to our resources
*/
	else if(request.url === "/updatecourse" && request.method ==="PUT"){
			response.writeHead(200, {"Content-Type" : "application/json"})
			response.write(`Update a course to our resources.`);
			response.end()
	}
	else if(request.url === "/archivecourse" && request.method ==="DELETE"){
			response.writeHead(200, {"Content-Type" : "application/json"})
			response.write(`Archive courses to our resources.`);
			response.end()
	}


}).listen(port);

console.log(`Server is running at port ${port}!`);

// nodemon server.js
// localhost:4000
// localhost:4000/profile
// localhost:4000/courses
// localhost:4000/addcourse
// localhost:4000/deletecourse
// localhost:4000/archivecourse